package russianfarmermultiplication;

import java.util.ArrayList;
import java.util.List;

public class App 
{
    public static void main( String[] args )
    {
		App app = new App();
		for(String row : app.draw(47, 42)){
			System.out.println(row);
		}
    }

	public int mul(int left, int right) {
		int result=0;
		while(left>=1){
			if(!isDivisible(left)){
				result+=right;
			}
			left = halve(left);
			right = duplicate(right);
		} 
		return result;
	}

	public int halve(int value) {
		return Math.floorDiv(value, 2);
	}

	public int duplicate(int value) {
		return value*2;
	}

	public boolean isDivisible(int value) {
		return value%2==0;
	}

	public List<String> draw(int left, int right) {
		List<String> list = new ArrayList<>();
		int mulLineLength = mulLineLength(left, right);
		int firstLeft=left;
		int firstRight=right;
		list.add(drawExercise(left, right));
		list.add(drawExerciseLine(mulLineLength));
		while(left>=1){
			list.add(drawSubstep(left, right, mulLineLength, firstLeft));
			left = halve(left);
			right = duplicate(right);
		}
		list.add(drawMulResultLine(mulLineLength));
		list.add(drawMulResult(mul(firstLeft, firstRight), mulLineLength));
		return list;
	}

	public String drawExerciseLine(int mulLineLength){
		StringBuilder builder = new StringBuilder();

		for(int i=0; i<mulLineLength; ++i){
			builder.append("-");
		}
		return builder.toString();
	}
	
	public String drawExercise(int left, int right){
		
		int mulLineLength = mulLineLength(left, right);
		int whitespaces = mulLineLength-Integer.toString(left).length()-Integer.toString(right).length();

		StringBuilder builder = new StringBuilder();

		builder.append(left);
		for(int i=0; i<whitespaces; ++i){
			if(Math.floorDiv(whitespaces, 2)==i){
				builder.append("*");
			}
			else{
				builder.append(" ");
			}
		}
		builder.append(right);

		return builder.toString();
	}

	public int mulLineLength(int left, int right){
		int mulResultLength = Integer.toString(left*right).length();
		return mulResultLength*2+1;
	}

	public String drawSubstep(int left, int right, int mulLineLength, int firstLeft) {

		int digitsFirstLeft = Integer.toString(firstLeft).length();
		int startWhitespaces = digitsFirstLeft-Integer.toString(left).length();

		int whitespaces = mulLineLength-Integer.toString(left).length()-Integer.toString(right).length()-startWhitespaces;

		StringBuilder builder = new StringBuilder();

		while(startWhitespaces>0){
			builder.append(" ");
			--startWhitespaces;
		}

		builder.append(left);
		for(int i=0; i<whitespaces; ++i){
			builder.append(" ");
		}
		builder.append(right);

		return builder.toString();
	}

	public String drawMulResultLine(int mulLineLength) {
		StringBuilder builder = new StringBuilder();

		for(int i=0; i<mulLineLength; ++i){
			builder.append("=");
		}
		return builder.toString();
	}

	public String drawMulResult(int mulResult, int mulLineLength) {
		int whitespaces = mulLineLength-Integer.toString(mulResult).length();
		StringBuilder builder = new StringBuilder();

		for(int i=0; i<whitespaces; ++i){
			builder.append(" ");
		}

		builder.append(Integer.toString(mulResult));

		return builder.toString();
	}
	
}
