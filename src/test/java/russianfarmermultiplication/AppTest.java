package russianfarmermultiplication;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class AppTest 
{
    @Test
    public void mulTest(){
        App app = new App();
        int actual = app.mul(47, 42);
        assertTrue(1974==actual);
    }

    @Test
    public void halveTest(){
        App app = new App();
        int actual = app.halve(47);
        assertTrue(23==actual);
    }

    @Test
    public void halve1Test(){
        App app = new App();
        int actual = app.halve(1);
        assertTrue(0==actual);
    }

    @Test
    public void duplicateTest(){
        App app = new App();
        int actual = app.duplicate(42);
        assertTrue(84==actual);
    }

    @Test
    public void isDivisibleTrueTest(){
        App app = new App();
        boolean isDivisible = app.isDivisible(2);
        assertTrue(isDivisible);
    }

    @Test
    public void isDivisibleFalseTest(){
        App app = new App();
        boolean isDivisible = app.isDivisible(47);
        assertFalse(isDivisible);
    }

    @Test
    public void drawTest(){
        App app = new App();
        List<String> actual = app.draw(47, 42);

        List<String> expected = Arrays.asList(
            "47  *  42", 
            "---------",
            "47     42",
            "23     84",
            "11    168",
            " 5    336",
            " 2    672",
            " 1   1344",
            "=========",
            "     1974"
        );

        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void drawSubstep47_42Test(){
        App app = new App();
        int mulLineLength = app.mulLineLength(47, 42);
        String actual = app.drawSubstep(47, 42, mulLineLength, 47);

        String expected = "47     42";

        assertTrue(expected.equals(actual));
    }

    @Test
    public void drawSubstep1_1344Test(){
        App app = new App();
        int mulLineLength = app.mulLineLength(47, 42);
        String actual = app.drawSubstep(1, 1344, mulLineLength, 47);

        String expected = " 1   1344";

        assertTrue(expected.equals(actual));
    }

    @Test
    public void drawMulResultLineTest(){
        App app = new App();
        int mulLineLength = app.mulLineLength(47, 42);
        String actual = app.drawMulResultLine(mulLineLength);

        String expected = "=========";

        assertTrue(expected.equals(actual));
    }

    @Test
    public void drawMulResultTest(){
        App app = new App();
        int mulLineLength = app.mulLineLength(47, 42);
        String actual = app.drawMulResult(1974, mulLineLength);

        String expected = "     1974";

        assertTrue(expected.equals(actual));
    }
}
